from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)
db = SQLAlchemy(app)
migrate = Migrate(app, db)


class Book(db.Model):
    __tablename__ = 'book'
    id = db.Column(db.Integer, primary_key=True)
    isbn = db.Column(db.String(), unique=True, nullable=False)
    title = db.Column(db.String(), nullable=False)
    author = db.Column(db.String(), nullable=False)
    create_date = db.Column(db.DateTime(), nullable=False)
    page_count = db.Column(db.Integer(), nullable=False)
    is_available = db.Column(db.Boolean(), nullable=False)

    def __init__(self, isbn, page_count, title, author, create_date, is_available=True):
        self.isbn = isbn
        self.page_count = page_count
        self.title = title
        self.author = author
        self.create_date = create_date
        self.is_available = is_available

    def insert(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def update(self):
        db.session.commit()
