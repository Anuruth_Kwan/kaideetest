from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)
db = SQLAlchemy(app)
migrate = Migrate(app, db)


class Borrowing(db.Model):
    __tablename__ = 'borrowing'
    id = db.Column(db.Integer, primary_key=True)
    member_id = db.Column(db.Integer)
    book_id = db.Column(db.Integer)
    borrow_date = db.Column(db.DateTime(), nullable=False)
    return_date = db.Column(db.DateTime(), nullable=True)
    create_date = db.Column(db.DateTime())
    last_update = db.Column(db.DateTime())

    def __init__(self, member_id, book_id, borrow_date, create_date, last_update, return_date=None):
        self.member_id = member_id
        self.book_id = book_id
        self.borrow_date = borrow_date
        self.create_date = create_date
        self.last_update = last_update
        if return_date is not None:
            self.return_date = return_date

    def insert(self):
        db.session.add(self)
        db.session.commit()

    def update(self):
        db.session.commit()
