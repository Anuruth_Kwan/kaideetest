from api.models.book import Book
from datetime import datetime


def get_response(code, message, data=None):
    if data is None:
        return {
            'status_code': code,
            'message': message,
        }
    return {
        'status_code': code,
        'message': message,
        'data': data
    }


class BookService:
    def __init__(self):
        print("__BookService instance was created__")

    def get_book_by_id(self, book_id):
        return Book.query.get(book_id)

    def get_books(self):
        return Book.query.all()

    def get_respond_book_by_id(self, book_id):
        book = self.get_book_by_id(book_id)
        if book is None:
            return get_response(201, "Book not found.")
        else:
            message = "Get book successful."
            book_response = self.assemble_book_response(book)
            return get_response(200, book_response, message)

    def get_respond_books(self):
        books = self.get_books()
        books_response = []
        if len(books) > 0:
            books_response = [self.assemble_book_response(item) for item in books]
        message = "Get books successful."
        return get_response(200, message, books_response)

    def assemble_book_response(self, book):
        return {
            "id": book.id,
            "isbn": book.isbn,
            "title": book.title,
            "author": book.author,
            "page_count": book.page_count,
            "is_available": book.is_available
        }

    def create_new_book(self, req_json):
        invalid_fields = self.validate_new_book_fields(req_json)
        if len(invalid_fields) > 0:
            invalid_data = ','.join(map(str, invalid_fields))
            message = "Invalid required fields: " + invalid_data
            return get_response(202, message)

        date_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        try:
            book = Book(
                req_json.get('isbn'),
                req_json.get('page_count'),
                req_json.get('title'),
                req_json.get('author'),
                date_time)
            book.insert()
            return get_response(200, "Book has been created.", self.assemble_book_response(book))
        except Exception as e:
            print(e)
            return get_response(204, "An exception occurred")

    def validate_new_book_fields(self, req_json):
        invalid_data = []
        if self.is_empty(req_json.get('isbn')):
            invalid_data.append("isbn")
        if self.is_empty(req_json.get('page_count')):
            invalid_data.append("page_count")
        if self.is_empty(req_json.get('title')):
            invalid_data.append("title")
        return invalid_data

    def is_empty(self, data):
        return data == "" or data is None

    def delete_book(self, book_id):
        book = Book.query.get(book_id)
        if book is None:
            get_response(201, 'Book not found.')
        try:
            book.delete()
            return get_response(200, "Book has been deleted.")
        except Exception as e:
            print(e)
            return get_response(204, "An exception occurred")

    def update_book(self, book_id, req_json):
        book = Book.query.get(book_id)
        if book is None:
            return get_response(201, "Book not found.")
        self.set_updated_book(book, req_json)
        try:
            book.update()
            return get_response(200, "Book has been updated.")
        except Exception as e:
            print(e)
            return get_response(204, "An exception occurred")

    def set_updated_book(self, book, req_json):
        if not self.is_empty(req_json.get('isbn')):
            book.isbn = req_json.get('isbn')
        if not self.is_empty(req_json.get('title')):
            book.title = req_json.get('title')
        if not self.is_empty(req_json.get('author')):
            book.author = req_json.get('author')
        if not self.is_empty(req_json.get('page_count')):
            book.page_count = req_json.get('page_count')
        if not self.is_empty(req_json.get('is_available')):
            book.is_available = req_json.get('is_available')
