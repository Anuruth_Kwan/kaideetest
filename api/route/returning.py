from http import HTTPStatus
from flask import Blueprint, request
from flasgger import swag_from
from api.services.returning_service import ReturningService

returning_api = Blueprint('api/returning', __name__)


@returning_api.route('/', methods=['POST'])
def return_book():
    req_json = request.json
    res_data = ReturningService().return_book(req_json)
    return res_data, 200
