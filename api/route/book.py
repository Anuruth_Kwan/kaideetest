from http import HTTPStatus
from flask import Blueprint, request, jsonify
from flasgger import swag_from
from flask_sqlalchemy import SQLAlchemy
from api.services.book_service import BookService

book_api = Blueprint('api/books', __name__)


@book_api.route('/', methods=['GET'])
def get_books():
    response_data = BookService().get_respond_books()
    return response_data, 200


@book_api.route('/<book_id>', methods=['GET'])
def get_book(book_id):
    response_data = BookService().get_respond_book_by_id(book_id)
    return response_data, 200


@book_api.route('/', methods=['POST'])
def crate_book():
    req_json = request.json
    response_data = BookService().create_new_book(req_json)
    return jsonify(response_data), 200


@book_api.route('/<book_id>', methods=['DELETE'])
def delete_book(book_id):
    response_data = BookService().delete_book(book_id)
    return jsonify(response_data), 200


@book_api.route('/<book_id>', methods=['PATCH'])
def update_book(book_id):
    res_json = request.json
    response_data = BookService().update_book(book_id, res_json)
    return jsonify(response_data), 200
