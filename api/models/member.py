from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

app = Flask(__name__)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

class Member(db.Model):
    __tablename__ = 'member'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String())
    first_name = db.Column(db.String())
    last_name = db.Column(db.String())
    tel = db.Column(db.String())
    create_date = db.Column(db.DateTime(), nullable=False)
    last_update = db.Column(db.DateTime(), nullable=False)

    def __init__(self, username, first_name, last_name, tel, create_date, last_update):
        self.username = username
        self.first_name = first_name
        self.last_name = last_name
        self.tel = tel
        self.create_date = create_date
        self.last_update = last_update