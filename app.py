from flask import Flask
from flasgger import Swagger
from flask_sqlalchemy import SQLAlchemy
from os import environ
from api.route.book import book_api
from api.route.borrowing import borrowing_api
from api.route.returning import returning_api
from api.route.member import member_api
import json
db = SQLAlchemy()


def create_app():
    app = Flask(__name__)

    f = open('swagger.json')
    app.config['SWAGGER'] = json.load(f)
    swagger = Swagger(app)
    # Initialize Config
    app.config['SQLALCHEMY_DATABASE_URI'] = environ['DATABASE_URL']
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    app.register_blueprint(book_api, url_prefix='/api/books')
    app.register_blueprint(borrowing_api, url_prefix='/api/borrowing')
    app.register_blueprint(returning_api, url_prefix='/api/returning')
    app.register_blueprint(member_api, url_prefix='/api/members')
    db.init_app(app)
    return app


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=5000, type=int, help='port to listen on')
    args = parser.parse_args()
    port = args.port
    app = create_app()

    app.run(host='127.0.0.1', port=port)
