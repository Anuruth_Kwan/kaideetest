from datetime import datetime
from api.models.borrowing import Borrowing
from api.services.book_service import BookService
from api.services.member_service import MemberService
from sqlalchemy import desc


def get_response(code, message, data=None):
    if data is None:
        return {
            'status_code': code,
            'message': message,
        }
    return {
        'status_code': code,
        'message': message,
        'data': data
    }


class BorrowingService:
    def __init__(self):
        print("__BorrowingService instance was created__")

    def list_borrowing_by_book_id(self, book_id):
        return Borrowing.query.filter(Borrowing.book_id == book_id).order_by(desc(Borrowing.borrow_date)).all()

    def get_respond_borrowings(self, book_id):
        book = BookService().get_book_by_id(book_id)
        if book is None:
            return get_response(201, "Book entity not found.")

        book_borrowings = self.list_borrowing_by_book_id(book_id)

        books_response = []
        if len(book_borrowings) > 0:
            books_response = [self.assemble_borrowing_response(item, book, item.member_id) for item in book_borrowings]
        message = "Get books borrowing history successful."
        return get_response(200, message, books_response)

    def create_borrowing(self, req_json):
        invalid_fields = self.validate_req_borrowing_fields(req_json)
        if len(invalid_fields) > 0:
            invalid_data = ','.join(map(str, invalid_fields))
            message = "Invalid required fields: " + invalid_data
            return get_response(202, message)
        book_id = req_json.get('book_id')
        book = BookService().get_book_by_id(book_id)
        if book is None:
            return get_response(201, "Book entity not found.")
        elif not book.is_available:
            return get_response(203, "Book is not available.")
        member_id = req_json.get('member_id')
        member = MemberService().get_member_by_id(member_id)
        if member is None:
            return get_response(201, "Member entity not found.")

        date_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        try:
            borrowing = Borrowing(
                member_id,
                book_id,
                date_time,
                date_time,
                date_time)
            borrowing.insert()
            book.is_available = False
            book.update()
            return get_response(200, "Borrowing has been created.",
                                self.assemble_created_borrowing_response(borrowing, book, member))
        except Exception as e:
            print(e)
            return get_response(204, "An exception occurred")

    def validate_req_borrowing_fields(self, req_json):
        invalid_data = []
        if self.is_empty(req_json.get('member_id')):
            invalid_data.append("member_id")
        if self.is_empty(req_json.get('book_id')):
            invalid_data.append("book_id")
        return invalid_data

    def assemble_borrowing_response(self, borrowing, book, member_id):
        member = MemberService().get_member_by_id(member_id)
        return {
            "id": borrowing.id,
            "borrow_date": borrowing.borrow_date,
            "return_date": borrowing.return_date,
            "create_date": borrowing.create_date,
            "last_update": borrowing.last_update,
            "book_detail": {
                "isbn": book.isbn,
                "page_count": book.page_count,
                "title": book.title,
                "author": book.author,
                "create_date": book.create_date,
            },
            "member_detail": {
                "fullname": member.first_name + ' ' + member.last_name
            }
        }

    def assemble_created_borrowing_response(self, borrowing, book, member):
        return {
            "id": borrowing.id,
            "borrow_date": borrowing.borrow_date,
            "create_date": borrowing.create_date,
            "last_update": borrowing.last_update,
            "book_detail": {
                "isbn": book.isbn,
                "page_count": book.page_count,
                "title": book.title,
                "author": book.author,
                "create_date": book.create_date,
            },
            "member_detail": {
                "fullname": member.first_name + ' ' + member.last_name
            }
        }

    def is_empty(self, data):
        return data == "" or data is None
