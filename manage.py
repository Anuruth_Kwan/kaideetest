from os import environ
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
from datetime import date
import requests

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = environ['DATABASE_URL']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)


class Book(db.Model):
    __tablename__ = 'book'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    isbn = db.Column(db.String(), unique=True, nullable=False)
    title = db.Column(db.String(), nullable=False)
    author = db.Column(db.String())
    create_date = db.Column(db.DateTime())
    page_count = db.Column(db.Integer(), nullable=False)
    is_available = db.Column(db.Boolean, nullable=False, default=True)

    def __init__(self, isbn, page_count, title, author, create_date):
        self.isbn = isbn
        self.page_count = page_count
        self.title = title
        self.author = author
        self.create_date = create_date


class Member(db.Model):
    __tablename__ = 'member'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String())
    first_name = db.Column(db.String())
    last_name = db.Column(db.String())
    tel = db.Column(db.String())
    create_date = db.Column(db.DateTime(), nullable=False)
    last_update = db.Column(db.DateTime(), nullable=False)

    def __init__(self, username, first_name, last_name, tel, create_date, last_update):
        self.username = username
        self.first_name = first_name
        self.last_name = last_name
        self.tel = tel
        self.create_date = create_date
        self.last_update = last_update


class Borrowing(db.Model):
    __tablename__ = 'borrowing'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    member_id = db.Column(db.Integer)
    book_id = db.Column(db.Integer)
    borrow_date = db.Column(db.DateTime(), nullable=False)
    return_date = db.Column(db.DateTime())
    create_date = db.Column(db.DateTime())
    last_update = db.Column(db.DateTime())


@manager.command
def seed():
    init_members()
    get_books()


def get_books():
    url = 'https://www.googleapis.com/books/v1/volumes?q=isbn&maxResults=40'
    res = requests.get(url, headers={"Content-Type": "application/json"})
    books_volume = res.json()
    if res.status_code == 200:
        assemble_isbn(books_volume['items'])


def assemble_isbn(items):
    today = date.today().strftime("%Y-%m-%d %H:%M:%S")
    for item in items:
        book_info = item['volumeInfo']
        isbn = get_isnb_10(book_info.get('industryIdentifiers'))
        author = get_author(book_info.get('authors'))
        page_count = book_info.get('pageCount')
        if isbn == '-1' or page_count is None:
            continue
        else:
            book = Book(isbn, page_count, book_info.get('title'), author, today)
            db.session.add(book)
            db.session.commit()


def get_isnb_10(industry_ids):
    if industry_ids is None:
        return '-1'
    if len(industry_ids) == 1:
        return industry_ids[0]['identifier']

    for item in industry_ids:
        if item['type'] == 'ISBN_10':
            return item['identifier']


def get_author(authors):
    if authors is None:
        return authors
    return authors[0]


def init_members():
    first = Member('Tom', 'Thomas', 'Edison', '0664589588', '2011-05-16 00:00:00', '2011-05-16 00:00:00')
    second = Member('Alex', 'Andrew', 'Edward', '0864544584', '2021-06-24 00:00:00', '2021-06-24 00:00:00')
    third = Member('Anna', 'Anna', 'Hebrew', '0698986524', '2021-07-05 00:00:00', '2021-07-05 00:00:00')
    db.session.add(first)
    db.session.add(second)
    db.session.add(third)
    db.session.commit()


if __name__ == '__main__':
    manager.run()
