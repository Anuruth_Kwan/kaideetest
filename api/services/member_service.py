from api.models.member import Member


def get_response(code, message, data=None):
    if data is None:
        return {
            'status_code': code,
            'message': message,
        }
    return {
        'status_code': code,
        'message': message,
        'data': data
    }


class MemberService:
    def __init__(self):
        print('__MemberService instance was created__')

    def get_member_by_id(self,member_id):
        return Member.query.get(member_id)

    def get_members(self):
        return Member.query.all()

    def get_respond_members(self):
        members = self.get_members()
        member_response = []
        if len(members) > 0:
            member_response = [self.assemble_member_response(item) for item in members]
        message = "Get members successful."
        return get_response(200, message, member_response)

    def get_respond_member_by_id(self, member_id):
        member = self.get_member_by_id(member_id)
        if member is None:
            return get_response(201, "member not found.")
        else:
            message = "Get member successful."
            member_response = self.assemble_member_response(member)
            return get_response(200, member_response, message)

    def assemble_member_response(self, member):
        return {
            "id": member.id,
            "tel": member.tel,
            "username": member.username,
            "first_name": member.first_name,
            "last_name": member.last_name,
            "create_date": member.create_date,
            "last_update": member.last_update
        }
