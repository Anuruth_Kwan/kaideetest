from http import HTTPStatus
from flasgger import swag_from
from flask import Blueprint
from api.services.member_service import MemberService

member_api = Blueprint('api/members', __name__)


@member_api.route('/', methods=['GET'])
def get_members():
    response_data = MemberService().get_respond_members()
    return response_data, 200


@member_api.route('/<member_id>', methods=['GET'])
def get_member(member_id):
    response_data = MemberService().get_respond_member_by_id(member_id)
    return response_data, 200
