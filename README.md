# Flask API For Library App

Sample API layout structure to be used as a baseline for other apps

## Set Up

1. Check out the code
2. Create virtual env (optional)
   1. install virtual env ```pip install virtualenv```
   2. create virtual env ```virtualenv venv```
   3. activate the evn  ```source ./venv/bin/activate```
3. Install requirements
    ```
    pip install -r requirements.txt 
    ```
   
4. Set environment variable for database connection
   1. `DATABASE_URL` for connection string
   


5. initial database information, please run serialize order
   ```
   python3 manage.py db init
   python3 manage.py db migrate
   python3 manage.py db upgrade
   python3 manage.py seed
   ```
6. Start the server with:
    ```
    python -m flask run
    ```

7. Visit http://127.0.0.1:5000/books for the books api

8. Visit http://127.0.0.1:5000/apidocs for the swagger documentation