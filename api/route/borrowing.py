from http import HTTPStatus
from flask import Blueprint, request
from flasgger import swag_from
from api.services.borrowing_service import BorrowingService

borrowing_api = Blueprint('api/borrowing', __name__)


@borrowing_api.route('/', methods=['POST'])
def borrow_book():
    req_json = request.json
    res_data = BorrowingService().create_borrowing(req_json)
    return res_data, 200


@borrowing_api.route('/<book_id>', methods=['GET'])
def list_borrowing_book(book_id):
    res_data = BorrowingService().get_respond_borrowings(book_id)
    return res_data, 200
