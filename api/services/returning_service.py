from datetime import datetime
from api.models.borrowing import Borrowing
from api.services.book_service import BookService
from api.services.member_service import MemberService
from sqlalchemy import desc


def get_response(code, message, data=None):
    if data is None:
        return {
            'status_code': code,
            'message': message,
        }
    return {
        'status_code': code,
        'message': message,
        'data': data
    }


class ReturningService:
    def __init__(self):
        print('__ReturningService instance was created__')

    def get_borrowed_book_with_book_id_and_member_id(self, book_id, member_id):
        return Borrowing.query.filter(Borrowing.book_id == book_id, Borrowing.member_id == member_id).order_by(
            desc(Borrowing.borrow_date)).first()

    def return_book(self, req_json):
        book_id = req_json.get('book_id')
        member_id = req_json.get('member_id')
        if self.is_empty(book_id) or self.is_empty(member_id):
            return get_response(201, 'Invalid require fields')

        book = BookService().get_book_by_id(book_id)
        if book is None:
            return get_response(201, "Book entity not found.")

        member = MemberService().get_member_by_id(member_id)
        if member is None:
            return get_response(201, "Member entity not found.")

        borrowed_book = self.get_borrowed_book_with_book_id_and_member_id(book_id, member_id)
        if borrowed_book is None:
            return get_response(201, "Borrowing record not found.")
        date_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        borrowed_book.return_date = date_time
        try:
            borrowed_book.update()
            book.is_available = True
            book.update()
            return get_response(200, 'Your book has been returned')
        except Exception as e:
            print(e)
            return get_response(204, "An exception occurred")

    def validate_returning_book(self, req_json):
        invalid_data = []
        if self.is_empty(req_json.get('book_id')):
            invalid_data.append("book_id")
        return invalid_data

    def is_empty(self, data):
        return data == "" or data is None
